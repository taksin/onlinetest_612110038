﻿using System;
using UnityEngine;
using Photon.Pun;

public class PunStick : MonoBehaviourPun
{
    public static PunStick instant;
    private int stickamount =1 ;
    private void Start()
    {
        instant = this;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && PunPlayerstate.instant.pickup == true)
        {
            PunPlayerstate.instant.pickup = false;
            PunPlayerstate otherstick = other.gameObject.GetComponent<PunPlayerstate>();
            otherstick.PunRPCTakedstick(stickamount);
            
            photonView.RPC("PunRPCPickUp",RpcTarget.All);
        }
    }

    [PunRPC]
    private void PunRPCPickUp()
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if(!photonView.IsMine)
            return;
            
        PhotonNetwork.Destroy(this.gameObject);
    }
}
