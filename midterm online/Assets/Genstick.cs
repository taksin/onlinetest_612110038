﻿using UnityEngine;

public class Genstick
{
    public static Vector3 RandomPosition(float yOffset)
    {
        var spawnPosition = new Vector3(
            Random.Range(-150.0f, 150.0f),
            yOffset, Random.Range(-150.0f, 150.0f));
        return spawnPosition;
    }

    public static Quaternion RandomRotation()
    {
        var spawnRotation = Quaternion.Euler(
            0f,
            Random.Range(0, 180), 0.0f);
            return spawnRotation;
    }
}
