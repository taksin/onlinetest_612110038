﻿using System;
using System.Collections;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PunPlayerstate : MonoBehaviourPunCallbacks
{   public static PunPlayerstate instant;
    public float currentwarm;
    public float currentstick;
    [SerializeField]
    Slider fire;
    public Text Stick;
    [SerializeField] private float leave;
    public float time;
    [SerializeField] private GameObject dead;
    [SerializeField] private GameObject win;
    public bool pickup = false;
    public GameObject title;
    public timer connect;
   
    private void Start()
    {
        instant = this;
        Stick.text = "";
        GetComponent<timer>();
    }
    [PunRPC]
    private void Update()
    {
        fire.value = currentwarm;
        currentwarm += Time.deltaTime * 2;
        time += Time.deltaTime;
        if (currentwarm <= 0)
        {
            currentwarm = 0;
        }

        if (currentwarm >= 100)
        {
            currentwarm = 100;
        }

        if (currentwarm >= 100)
        {
            dead.SetActive(true);
            leave += Time.deltaTime;
        }
        if (time >= 4)
        {
            title.SetActive(false);
        }
        if (leave >= 3)
        {
            PhotonNetwork.LeaveRoom();
        }
        if (connect.winshow == true)
        {
            win.SetActive(true);
            Debug.Log("Win");
            leave += Time.deltaTime;
        }
        if (Input.GetMouseButtonUp(0))
        {
            pickup = false;
            Stick.text = "";
        }
        
    }

    public void OnGUI()
    {
        if(photonView.IsMine)
            GUI.Label(new Rect(0,0,300,50),"Player Temp : " + Mathf.Round(currentwarm).ToString()  );
        
        if(photonView.IsMine)
            GUI.Label(new Rect(0,30,300,50),"Stick Fuel : " + Mathf.Round(currentstick).ToString()+" / 5" );
    }

    public void Takeheal(int amount, int OwnerNetID)
    {
        if (photonView != null)
        {
            photonView.RPC("PunRPCTakedHeal",RpcTarget.All,amount,OwnerNetID);
        }
    }
    
    [PunRPC]
    public void PunRPCTakedHeal(int amount)
    {
        currentwarm -= amount;
    }
    [PunRPC]
    public void PunRPCTakedstick(int amount)
    {
        currentstick += amount;
        //currentstick = PhotonNetwork.PlayerList.Length;
    }
    
    [PunRPC]
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Stick") )
        {

            Stick.text = "Click leftmouse to pick";
            

                if (Input.GetMouseButtonDown(0) && currentstick <= 4)
                {
                    //Debug.Log("pickup");
                    pickup = true;
                }
        }

        if (other.gameObject.CompareTag("Stick")&& currentstick == 5)
        {

            Stick.text = "Your bag is full";
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Stick")||other.gameObject.CompareTag("Fire"))
        {
            
                pickup = false;
                Stick.text = "";
            
        }
    }
}
