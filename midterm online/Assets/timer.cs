﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using System;
//using System.Diagnostics.Eventing.Reader;

public class timer : MonoBehaviourPunCallbacks
{
    public delegate void CountdownTimerHasExpired();
    public static event CountdownTimerHasExpired OnCountdownTimerHasExpired;
    private bool isTimerRunning;
    private float startTime;
    [Header("Reference to a Text component for visualizing the countdown")]
    public Text Text;
    [Header("Countdown time in seconds")] public float Countdown = 30f;
    public float currentCountDown;
    public float currenttimetowin;
    public GameObject firelive;
    public bool warmend;
    public bool winshow;
    public void Start()
    {
        if (Text == null)
        {
            Debug.LogError("Reference to 'Text' is not set.Please set a valid reference.",this);
            return;
        }
        PunNetworkManager.OnFirstSetting += StartTime;
    }

    public void Update()
    {
        if(!isTimerRunning)
            return;
        float timer = (float) PhotonNetwork.Time - startTime;
        currentCountDown = Countdown - timer;
        Text.text = "" + CovertformatTime(currentCountDown);
        currenttimetowin = Countdown + timer;
        if (currenttimetowin >= 130)
        {
            winshow = true;
        }
        if (currentCountDown > 0.0f)
        {
            return;
        }
        isTimerRunning = false;
            firelive.SetActive(false);
            warmend = true;
            Text.text = string.Empty;
            if (OnCountdownTimerHasExpired != null)
            {
                OnCountdownTimerIsExpired();
            }
    }

    string CovertformatTime(float seconds)
    {
        double hh = Math.Floor(seconds / 3600),
            mm = Math.Floor(seconds / 60) % 60,
            ss = Math.Floor(seconds) % 60;
        return hh.ToString("00") + ":" +
               mm.ToString("00") + ":" +
               ss.ToString("00");
    }

    public void StartTime()
    {
        Hashtable props = new Hashtable
        {
            {
                PunGameSetting.START_GAMETIME, (float) PhotonNetwork.Time
            }
        };
        PhotonNetwork.CurrentRoom.SetCustomProperties(props);
    }
    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        GetStartTime(propertiesThatChanged);
    }

    public void addfire(int amount)
    {
        Countdown += amount;
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        GetStartTime(PhotonNetwork.CurrentRoom.CustomProperties);
    }

    public void GetStartTime(Hashtable propertiesThatChanged)
    {
        object startTimeFromProps;

        if (propertiesThatChanged.TryGetValue(PunGameSetting.START_GAMETIME, out startTimeFromProps))
        {
            isTimerRunning = true;
            startTime = (float) startTimeFromProps;
        }
    }
    

    private void OnCountdownTimerIsExpired()
    {
        if(PhotonNetwork.CurrentRoom == null)
            return;
        Hashtable props = new Hashtable
        {
            {PunGameSetting.GAMEOVER, true}
        };
        PhotonNetwork.CurrentRoom.SetCustomProperties(props);
    }

    public override void OnEnable()
    {
        base.OnEnable();
        OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        OnCountdownTimerHasExpired -= OnCountdownTimerIsExpired;
    }
}

