﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks ,IPunInstantiateMagicCallback
{
    public static GameObject LocalPlayerInstance;
    public Canvas slide;
    public Text show;
    private int stickshow;
    public int m_time = 10;
    public static PunUserNetControl singelton;
    public PunFire connect;
    public timer connect2;
    public void Start()
    {
        slide = GetComponent<Canvas>();
        singelton = this;
        connect = GetComponent<PunFire>();
        connect2 = GetComponent<timer>();
        
    }
    public void Update()
    {
        //connect.addfire = false;
        if (connect.addfire == true)
        {
            OnFire();
            Debug.Log("a");
        }
    }
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        info.Sender.TagObject = this.gameObject;
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
            //ChangeColorProperites();
            
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
            GetComponent<FirstPersonController>().enabled = false;
            GetComponent<PunPlayerstate>().enabled = false;
            GetComponent<AudioSource>().enabled = false;
            slide.enabled = false;
        }
    }

    /*private void ChangeColorProperites()
    {
        Hashtable props = new Hashtable
        {
            {PunGameSetting.PLAYER_COLOR,Random.Range(1,4)}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    } */

    void OnFire()
    {
        Hashtable Onfire = new Hashtable
        {
            {PunGameSetting.PLATER_LIVES, connect.addfire}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(Onfire);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (changedProps.ContainsKey(PunGameSetting.PLATER_LIVES) && targetPlayer.ActorNumber == photonView.ControllerActorNr)
        {
            object fire;
            if (changedProps.TryGetValue(PunGameSetting.PLATER_LIVES,out fire))
            {
                connect2.addfire(m_time);
            }
            return;
        }
    }
    /*public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (changedProps.ContainsKey(PunGameSetting.PLAYER_READY) &&
            targetPlayer.ActorNumber == photonView.ControllerActorNr)
        {
            object colors;
            if (changedProps.TryGetValue(PunGameSetting.PLAYER_READY,out colors))
            {
               // GetComponent<MeshRenderer>().material.color = PunGameSetting.GetColor((int) colors);
               connect2.addfire();
               Debug.Log("B");
            }

            object stick;
            if (changedProps.TryGetValue(PunPlayerstate.instant.currentstick, out stick))
            {
                GetComponent<PunPlayerstate>().currentstick  = stickshow;
            }
            return;
        }
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    { 
        object addfuel;
        if(propertiesThatChanged.TryGetValue(PunGameSetting.PLAYER_READY,out addfuel))
        {
            connect2.addfire();
        }
    }*/
    
}
