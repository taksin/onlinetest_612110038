﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunGameSetting 
{
    public const float MIN_SPAWN_TIME = 5.0f;
    public const float MAX_SPAWN_TIME = 10.0f;
    
    public const float PLAYER_RESPAWN_TIME = 4.0f;
    
    public const string PLATER_LIVES = "PlayerLives";
    public const int PLAYER_MAX_LIVES = 3;
    
    public const string PLAYER_READY = "IsPlayerReady";
    public const string PLAYER_LOADED_LEVEL = "PlayerLoadLevel";
    public const string PLAYER_COLOR = "PlayerColor";
    
    
    public const string START_GAMETIME = "GameTime";
    public const string GAMEOVER = "IsGameOver";

    public static Color GetColor(int colorChoice)
    {
        switch (colorChoice)
        {
            case  1: return  Color.red;
            case  2: return  Color.blue;
            case  3: return  Color.green;
            case  4: return  Color.yellow;
        }
        return Color.black;
    }
}
