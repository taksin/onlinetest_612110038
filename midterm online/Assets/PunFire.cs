﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using System;

public class PunFire : MonoBehaviourPunCallbacks
{
    public timer connent;
    public int firewarm = 1;
    public bool addfire;
    private void Start()
    {
        connent = GetComponent<timer>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && connent.warmend == false)
        {
            PunPlayerstate.instant.Stick.text = "Click leftmouse to add fuel";
            PunPlayerstate otherwarm = other.gameObject.GetComponent<PunPlayerstate>();
            otherwarm.PunRPCTakedHeal(firewarm/2);
            if (PunPlayerstate.instant.currentstick >= 1 && Input.GetMouseButtonDown(0))
            {
                PunPlayerstate.instant.currentstick -= 1;
                connent.Countdown += 10;
            }
        }
    }
}
