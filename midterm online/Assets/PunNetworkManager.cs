﻿
using System;
using ExitGames.Client.Photon;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class PunNetworkManager : ConnectAndJoinRandom
{
    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public static PunNetworkManager singleton;
    public GameObject GamePlayerPrefab;

    public bool isGameStart = false;
    public bool isFirstSetting = false;
    public bool isGameOver = false;
    public GameObject stickPrefab;
    public GameObject tree1;
    public GameObject tree2;
    public GameObject tree3;
    public int numberOfstick = 50;
    public int numbertree = 5;
    float m_count = 0;
    public float m_CountDownDropstick = 1;
    public bool firstwork;
    
    public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;

    public delegate void FirstSetting();
    public static event FirstSetting OnFirstSetting;

    public void Awake()
    {
        singleton = this;
        OnPlayerSpawned += SpawnPlayer;
        OnFirstSetting += dropstick;
        
    }

    private void dropstick()
    {
        if (isFirstSetting == false)
        {
            isFirstSetting = true;
            for (int i = 0; i < numberOfstick; i++)
            {
                PhotonNetwork.InstantiateRoomObject(stickPrefab.name
                    , Genstick.RandomPosition(8f)
                    , Genstick.RandomRotation()
                    , 0);
            }
            for (int i = 0; i < numbertree; i++)
            {
                PhotonNetwork.InstantiateRoomObject(tree1.name
                    , Genstick.RandomPosition(0f)
                    , Genstick.RandomRotation()
                    , 0);
            }
            for (int i = 0; i < numbertree; i++)
            {
                PhotonNetwork.InstantiateRoomObject(tree2.name
                    , Genstick.RandomPosition(0f)
                    , Genstick.RandomRotation()
                    , 0);
            }
            for (int i = 0; i < numbertree; i++)
            {
                PhotonNetwork.InstantiateRoomObject(tree3.name
                    , Genstick.RandomPosition(0f)
                    , Genstick.RandomRotation()
                    , 0);
            }
            m_count = m_CountDownDropstick;
        }
        
            if (GameObject.FindGameObjectsWithTag("Stick").Length < numberOfstick)
            {
                //Debug.Log(""+ numberOfstick);
                m_count -= Time.deltaTime;
                if (m_count <= 0)
                {
                    m_count = m_CountDownDropstick;
                    PhotonNetwork.Instantiate(stickPrefab.name
                        ,Genstick.RandomPosition(1f)
                        , Genstick.RandomRotation()
                        , 0);
                }
            }
        
    }

    public override void OnJoinedRoom()
    {
        
            base.OnJoinedRoom();
            Camera.main.gameObject.SetActive(false);


            if (PunUserNetControl.LocalPlayerInstance == null)
            {
                PunNetworkManager.singleton.SpawnPlayer();
            }
        

    }

    public void SpawnPlayer()
    {
        isGameStart = true;
        PhotonNetwork.Instantiate(GamePlayerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
            
        

    }

    private void Update()
    {
        if (PhotonNetwork.IsMasterClient != true)
        {
            return;
        }
        
        if (isFirstSetting == false  )
        {
            firstwork = true;
            OnFirstSetting();
            isFirstSetting = true;
        }
        


    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
    }
}
